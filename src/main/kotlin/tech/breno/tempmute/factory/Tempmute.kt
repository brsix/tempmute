package tech.breno.tempmute.factory

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.entity.Player
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.collections.HashMap

data class Tempmute(
        val author: String,
        val player: Player,
        val time: Long
) {

    init {
        broadcast("""
            
             The player ${player.name} has been muted by
             $author with no reason.
             Time: $time
            
        """.trimIndent())
    }

    fun process() {
        Controller.muteController[player.uniqueId] = this
    }

}

object Controller {
    val muteController = HashMap<UUID, Tempmute>()
}

fun Player.tempmute(time: Long, author: String) = Tempmute(author, this, time)
fun Player.remaingTime()                       = Controller.muteController[uniqueId]
fun Player.unmute()                            = Controller.muteController.remove(uniqueId)

fun broadcast(text: String) = Bukkit.broadcastMessage("§c $text")