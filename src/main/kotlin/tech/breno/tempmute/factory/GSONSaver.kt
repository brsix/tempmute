package tech.breno.tempmute.factory

import com.google.gson.GsonBuilder

import org.bukkit.scheduler.BukkitRunnable
import java.io.*
import java.util.*

class Applier : BukkitRunnable() {

    override fun run() {
        var controller = Controller.muteController

        controller.forEach {
            var writer = FileWriter("/save/data.json")
            var gson = GsonBuilder().create()

            gson.toJson(controller, writer)
        }
    }

}

class Loader(val uuid: UUID) : BukkitRunnable() {

    override fun run() {
        summon()
    }

    fun summon(): Tempmute? {
        lateinit var jsonData: Tempmute

        var file = File("/save/data.json")
        lateinit var inputStream: InputStream

        var gson = GsonBuilder().enableComplexMapKeySerialization()
                .setPrettyPrinting().create()

        inputStream = FileInputStream(file)
        var streamReader = InputStreamReader(
                inputStream, "UTF-8"
        )

        jsonData = gson.fromJson(streamReader, Tempmute::class.java)
        streamReader.close()
        inputStream?.close()

        return jsonData.takeIf {
            it.player.equals(uuid)
        }
    }


}