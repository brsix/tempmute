package tech.breno.tempmute

import org.bukkit.plugin.java.JavaPlugin
import tech.breno.tempmute.factory.Applier

import tech.breno.tempmute.minecraft.commands.TempmuteCommand
import tech.breno.tempmute.minecraft.commands.UnmuteCommand
import tech.breno.tempmute.minecraft.listeners.PlayerChatListener
import tech.breno.tempmute.minecraft.listeners.PlayerJoinListener

class TempmutePlugin : JavaPlugin() {

    /**
     * @author brxnx
     * @discord brxnx#2818
     *
     * Plugin made for Senior Team freelance
     * test. All rights reserved.
     */

    override fun onEnable() {
        Applier().runTaskTimerAsynchronously(this, (10 * 60 * 20L), (10 * 60 * 20L))

        getCommand("tempmute")?.setExecutor(TempmuteCommand())
        getCommand("unmute")?.setExecutor(UnmuteCommand())

        PlayerChatListener()
        PlayerJoinListener()
    }

    companion object {
        var instance = getPlugin(TempmutePlugin::class.java)
    }

}