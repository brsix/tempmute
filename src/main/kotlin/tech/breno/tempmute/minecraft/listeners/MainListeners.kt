package tech.breno.tempmute.minecraft.listeners

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent
import org.bukkit.event.player.AsyncPlayerPreLoginEvent
import org.bukkit.event.player.PlayerJoinEvent
import tech.breno.tempmute.TempmutePlugin
import tech.breno.tempmute.factory.Controller
import tech.breno.tempmute.factory.Loader
import tech.breno.tempmute.factory.remaingTime
import tech.breno.tempmute.minecraft.commands.error

class PlayerChatListener : Listener {

    init {
        TempmutePlugin.instance.server.pluginManager
                .registerEvents(
                        this, TempmutePlugin.instance
                )
    }

    @EventHandler
    fun onChat(event: AsyncPlayerChatEvent) {
        var player  = event.player
        var message = event.message

        if (Controller.muteController.contains(player.uniqueId)) {
            player.error(
                    "You cannot speak because you are muted, time remaining: ${player.remaingTime()}"
            )
            event.isCancelled = true
        }
    }

}


class PlayerJoinListener : Listener {

    init {
        TempmutePlugin.instance.server.pluginManager
                .registerEvents(
                        this, TempmutePlugin.instance
                )
    }

    @EventHandler
    fun onJoin(event: AsyncPlayerPreLoginEvent) {
        var player = event.uniqueId

        if (!Controller.muteController.contains(player))
            Loader(player).runTaskAsynchronously(TempmutePlugin.instance)
    }

}