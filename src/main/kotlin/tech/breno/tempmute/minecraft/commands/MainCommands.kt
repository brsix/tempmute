package tech.breno.tempmute.minecraft.commands

import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import tech.breno.tempmute.factory.Controller

import tech.breno.tempmute.factory.tempmute
import tech.breno.tempmute.factory.unmute

fun CommandSender.error(text: String) = sendMessage("§c $text")
fun CommandSender.reply(text: String) = sendMessage("§a $text")

class TempmuteCommand : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (args.isEmpty() || args.size == 1) {
            sender.error("Args can't be empty, use: /tempmute <nick> <time>")
            return false
        }

        var target = Bukkit.getPlayer(args[0])
        var time = args[1].toLong()

        target?.apply {
            tempmute(time, sender.name).process()
        }

        return false
    }

}

class UnmuteCommand : CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) {
            sender.error("Args can't be empty, use: /unmute <nick>")
            return false
        }

        var target = Bukkit.getPlayer(args[0])

        if (Controller.muteController.contains(target?.uniqueId)) {
            target?.apply {
                unmute()
            }

            sender.reply("Success!")
        }

        return false
    }

}

